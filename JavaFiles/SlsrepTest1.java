package slsreps;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 * @author merry
 */
public class SlsrepTest 
{
   public static void main(String[] args) 
   {
      Scanner in = new Scanner(System.in);
      String response;
      Query query;
      List<Slsrep> mySlsreps;
      
      EntityManagerFactory emf = 
         Persistence.createEntityManagerFactory("SlsrepAppPU");
      EntityManager em = emf.createEntityManager();
      
      em.getTransaction().begin();
      System.out.print(
         "Do you want to create a new employee (Y or N)? ");
      response = in.next();
      if(response.equalsIgnoreCase("Y"))
      {
         System.out.print("Enter sales rep's number: ");
         Slsrep rep = new Slsrep(in.nextInt());
         System.out.print("Enter sales rep's last name: " );
         rep.setLastname(in.next());
         System.out.print("Enter sales rep's first name: " );
         rep.setFirstname(in.next());
         System.out.print("Enter sales rep's 5-digit zip code: " );
         rep.setZip(in.next());
         System.out.print("Enter sales rep's total commission: " );
         rep.setTotcomm(new BigDecimal(in.next()));
         System.out.print("Enter sales rep's commission rate: " );
         rep.setCommrate(new BigDecimal(in.next()));
         em.persist(rep);
      }
      em.getTransaction().commit();
      
      query = em.createNamedQuery("Slsrep.findAll");
      mySlsreps = query.getResultList();
      for(Slsrep myRep : mySlsreps)
      {
         System.out.println(myRep);
      }
      System.out.println();
      
      em.getTransaction().begin();
      System.out.print("Enter a rep number: " );
      Slsrep rep = em.find(Slsrep.class, in.nextInt());
      rep.setCommrate(rep.getCommrate().multiply(new BigDecimal("2")));   
   
      mySlsreps = query.getResultList();
      for(Slsrep myRep : mySlsreps)
      {
         System.out.println(myRep);
      }
      em.getTransaction().commit();
   }
}
