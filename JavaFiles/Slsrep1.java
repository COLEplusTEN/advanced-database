package slsreps;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author merry
 */
@Entity
@Table(name = "SLSREP")
@XmlRootElement
@NamedQueries(
{
   @NamedQuery(name = "Slsrep.findAll", query =
   "SELECT s FROM Slsrep s"),
   @NamedQuery(name = "Slsrep.findByRepnum", query =
   "SELECT s FROM Slsrep s WHERE s.repnum = :repnum"),
   @NamedQuery(name = "Slsrep.findByLastname", query =
   "SELECT s FROM Slsrep s WHERE s.lastname = :lastname"),
   @NamedQuery(name = "Slsrep.findByFirstname", query =
   "SELECT s FROM Slsrep s WHERE s.firstname = :firstname"),
   @NamedQuery(name = "Slsrep.findByZip", query =
   "SELECT s FROM Slsrep s WHERE s.zip = :zip"),
   @NamedQuery(name = "Slsrep.findByTotcomm", query =
   "SELECT s FROM Slsrep s WHERE s.totcomm = :totcomm"),
   @NamedQuery(name = "Slsrep.findByCommrate", query =
   "SELECT s FROM Slsrep s WHERE s.commrate = :commrate")
})
public class Slsrep implements Serializable
{

   private static final long serialVersionUID = 1L;
   @Id
   @Basic(optional = false)
   @Column(name = "REPNUM")
   private Integer repnum;
   @Column(name = "LASTNAME")
   private String lastname;
   @Column(name = "FIRSTNAME")
   private String firstname;
   @Column(name = "ZIP")
   private String zip;
   // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
   @Column(name = "TOTCOMM")
   private BigDecimal totcomm;
   @Column(name = "COMMRATE")
   private BigDecimal commrate;

   public Slsrep()
   {
   }

   public Slsrep(Integer repnum)
   {
      this.repnum = repnum;
   }

   public Integer getRepnum()
   {
      return repnum;
   }

   public void setRepnum(Integer repnum)
   {
      this.repnum = repnum;
   }

   public String getLastname()
   {
      return lastname;
   }

   public void setLastname(String lastname)
   {
      this.lastname = lastname;
   }

   public String getFirstname()
   {
      return firstname;
   }

   public void setFirstname(String firstname)
   {
      this.firstname = firstname;
   }

   public String getZip()
   {
      return zip;
   }

   public void setZip(String zip)
   {
      this.zip = zip;
   }

   public BigDecimal getTotcomm()
   {
      return totcomm;
   }

   public void setTotcomm(BigDecimal totcomm)
   {
      this.totcomm = totcomm;
   }

   public BigDecimal getCommrate()
   {
      return commrate;
   }

   public void setCommrate(BigDecimal commrate)
   {
      this.commrate = commrate;
   }

   @Override
   public int hashCode()
   {
      int hash = 0;
      hash += (repnum != null ? repnum.hashCode() : 0);
      return hash;
   }

   @Override
   public boolean equals(Object object)
   {
      // TODO: Warning - this method won't work in the case the id fields are not set
      if (!(object instanceof Slsrep))
      {
         return false;
      }
      Slsrep other = (Slsrep) object;
      if ((this.repnum == null && other.repnum != null) ||
         (this.repnum != null && !this.repnum.equals(other.repnum)))
      {
         return false;
      }
      return true;
   }

   @Override
   public String toString()
   {
      String name = firstname + " " + lastname;
      String comm = (totcomm.setScale(2, BigDecimal.ROUND_HALF_UP)).
         toPlainString();
      String rate = (commrate.setScale(2, BigDecimal.ROUND_HALF_UP)).
         toPlainString();
      return String.format(
         "%-5s %-20s %5s $%10s %10s", repnum, name, zip, comm, rate);
   }
}
