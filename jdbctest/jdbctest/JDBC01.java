package jdbctest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBC01 {

    private static final Logger LOG = Logger.getLogger(JDBC01.class.getName());

    public static void main(String[] args) {
        String driver = "oracle.jdbc.driver.OracleDriver";
        String url = "jdbc:oracle:thin:@198.209.246.200:1521:orcl";
        String username = "scott";
        String password = "tiger";
        Statement dbStatement = null;
        ResultSet dbResultSet = null;
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(url, username, password);
            LOG.info("Connection successful.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }

        String query
                = "select isbn, title, authFirstName, authLastName from tblBooks";
        LOG.log(Level.INFO, "Query: {0}", query);
        try {
            dbStatement = conn.createStatement();
            LOG.info("Statement created successfully.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        try {
            dbResultSet = dbStatement.executeQuery(query);
            LOG.info("Query executed correctly.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        LOG.info("================\n");
        displayResults(dbResultSet);
        LOG.info("================\n");
        try {
            dbStatement.close();
            LOG.info("Statement closed.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        try {
            conn.close();
            LOG.info("Connection closed.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
    } // end main

    public static void displayResults(ResultSet dbrs) {
        int count = 0;
        try {
            while (dbrs.next()) {
                System.out.printf("%-15s %-40s %1s %1s\n",
                        dbrs.getString("isbn"), dbrs.getString("title"),
                        dbrs.getString("authFirstName"),
                        dbrs.getString("authLastName"));
                count++;
            }
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        LOG.log(Level.INFO, "{0} records in result set\n", count);
    }
}
