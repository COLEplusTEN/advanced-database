package jdbctest;

import java.sql.*;
import java.util.logging.Logger;

public class JDBC02 {

    private static final Logger LOG = Logger.getLogger(JDBC02.class.getName());

    public static void main(String[] args) {
        Connection conn = DataSourceCreator.getConn();
        try {
            PreparedStatement updateZipCode = conn.prepareStatement(
                    "update tblStudent set zip = ? where city = ?");
            System.out.println("PreparedStatement created");

            updateZipCode.setString(1, "55555");
            updateZipCode.setString(2, "Maryville");
            updateZipCode.executeUpdate();

            System.out.println("PreparedStatement executed");
        } catch (SQLException e) {
            System.out.println("Problems with PreparedStatement");
        }
        try {

            //  CallableStatement updateGpaStmt =
            //  conn.prepareCall("call update_gpa(944, 1.5)");
            // OR
            // If you want to use parameters
            CallableStatement updateGpaStmt
                    = conn.prepareCall("call update_gpa(?,?)");
            updateGpaStmt.setInt(1, 944);
            updateGpaStmt.setDouble(2, 1.5);

            System.out.println("CallableStatement created");
            updateGpaStmt.executeUpdate();
            System.out.println("CallableStatement executed");
        } catch (SQLException ex) {
            System.out.println("Problems with CallableStatement");
        }

        if (conn != null) {
            try {
                conn.close();
                System.out.println("Connection closed. Successfully completed.");
            } catch (SQLException ex) {
                System.out.println("Problems closing connection" + ex);
            }
        }
    }
}
