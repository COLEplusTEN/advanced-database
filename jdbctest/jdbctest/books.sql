drop table booksAudit;
drop table booksRowAudit;
drop sequence booksAuditIdSequence;
drop sequence booksRowAuditIdSequence;
DROP TABLE tblBooks;

CREATE TABLE tblBooks(
ISBN varchar2(16),
AuthLastName varchar2(20),
AuthFirstName varchar2(20),
Title varchar2(40),
Price Number(8,2),
PubDate DATE,
CONSTRAINT tblBooks_ISBN_pk PRIMARY KEY (ISBN));

INSERT INTO tblBooks VALUES (
  '0029303303', 'Sowell', 'Thomas', 'Inside American Education', 18.95, 
  TO_DATE('2004-01-20', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES ( 
  '0132968983', 'Malamud', 'Carl', 'Exploring The Internet', 26.95, 
  TO_DATE('1996-04-02', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0192814419', 'Shakespeare', 'William', 'Macbeth', 20.50, 
  TO_DATE('1998-05-12', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0198534361', 'Berberian', 'Sterling', 'Linear Algebra', 9.95, 
  TO_DATE('1996-04-02', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0316542261', 'Mack', 'Alison', 'Dry All Night', 75.99, 
  TO_DATE('1996-04-02', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0658855399', 'Schwartz', 'Arnold', 'Dynamic Professional Selling', 25.00, 
  TO_DATE('1960-12-19', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0853348200', 'Vlitos', 'A.J.', 'Developments in Sweeteners', 17.50, 
  TO_DATE('1980-03-05', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0872876365', 'Lambert', 'Shirley', 'Clipart And Dynamic Designs', 45.00, 
  TO_DATE('1996-04-02', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0890795436', 'Wilson', 'Meg', 'Options For Girls', 9.95, 
  TO_DATE('1996-04-02', 'YYYY-MM-DD'));
INSERT INTO tblBooks VALUES (
  '0920437832', 'Williams', 'Henry', 'The Great Astronomers', 35.00, 
  TO_DATE('1996-04-02', 'YYYY-MM-DD'));

commit;
