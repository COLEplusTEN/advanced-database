package jdbctest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBC01A_P {

    private static final Logger LOG = Logger.getLogger(JDBC01A_P.class.getName());

    public static void main(String[] args) {
        Statement dbStatement = null;
        ResultSet dbResultSet = null;
        Connection conn = DataSourceCreator.getConn();
        System.out.println("Connection successful.");

        String query
                = "select isbn, authlastname, authfirstname, title "
                + "from tblBooks";
        System.out.printf("Query: {0}", query);
        try {
            dbStatement = conn.createStatement();
            System.out.printf("Statement created successfully.");
        } catch (SQLException ex) {
            System.out.printf("ERROR: Problems creating statement: {}", ex);
        }
        try {
            dbResultSet = dbStatement.executeQuery(query);
            System.out.println("INFO: Query executed correctly.");
        } catch (SQLException ex) {
            System.out.printf("ERROR: Problems executing statement: {}" + ex);
        }
        System.out.println();
        displayResults(dbResultSet);
        System.out.println();
        try {
            dbStatement.close();
            LOG.info("Statement closed.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        try {
            conn.close();
            LOG.info("Connection closed.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
    } // end main

    public static void displayResults(ResultSet dbrs) {
        int count = 0;
        try {
            while (dbrs.next()) {
                System.out.printf("%-15s %-40s %1s %1s\n",
                        dbrs.getString("isbn"), dbrs.getString("title"),
                        dbrs.getString("authFirstName"),
                        dbrs.getString("authLastName"));
                count++;
            }
        } catch (SQLException ex) {
            System.out.println(
                    "SQLException occurred while displaying results.\n"
                    + ex);
        }
        System.out.println();
        System.out.println(count + " records in result set");
    }

}
