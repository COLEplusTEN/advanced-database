package jdbctest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import oracle.jdbc.pool.OracleDataSource;

/**
 * Reads a db.properties file from the root project folder.
 */
public class DataSourceCreator {

    private static final Logger LOG = Logger.getLogger(DataSourceCreator.class.getName());
    private static OracleDataSource ods = null;
    private static String username = null;
    private static String password = null;
    private static String url = null;

    /**
     * DataSourceCreator test program.  Uses configuration information from 
     * dba.properties (for a sys as sysdba connection) and db.properties for 
     * a lower-level connection to a PDB (e.g. hr/hr).
     * 
     * @param args 
     */
    public static void main(String[] args) {
        initialize();
        unlockDBUser();
        initializeODS();
    }

    /**
     * Initialize an OracleDataSource based on information in
     * db.properties.
     */
    public static void initializeODS() {
        try {
            DataSourceCreator.ods = new OracleDataSource();
        } catch (SQLException ex) {
            Logger.getLogger(DataSourceCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
        ods.setURL(url);
        ods.setUser(username);
        ods.setPassword(password);
    }

    /** 
     * Read information from the db.properties file and set up the 
     * connection url. In 12c, we use / service.  In 11g, we use :SID.
     * If using 11g, you'll need to edit this to use a ":" rather than a "/".
     */
    public static void initialize() {
        Properties properties = null;
        try (FileInputStream fileInput = new FileInputStream(new File("db.properties"))) {
            properties = new Properties();
            properties.load(fileInput);
        } catch (FileNotFoundException e) {
            LOG.log(Level.SEVERE, null, e);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        Enumeration props = properties.keys();
        while (props.hasMoreElements()) {
            String key = (String) props.nextElement();
            String value = properties.getProperty(key);
            LOG.log(Level.INFO, "{0}: {1}", new Object[]{key, value});
        }
        final String ip = properties.getProperty("ip");
        final String port = properties.getProperty("port");
        final String service = properties.getProperty("service");
        username = properties.getProperty("username");
        password = properties.getProperty("password");
        url = "jdbc:oracle:thin:@127.0.0.1:" + port + "/" + service;
        LOG.log(Level.INFO, "Connection url={0}", url);

    }

    /**
     * Get a Connection object based on the configuration provided in db.properties.
     * @return a Connection object
     */
    public static Connection getConn() {
        try {
            initialize();
            unlockDBUser();
            initializeODS();
            return ods.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DataSourceCreator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * Creates a connection as "sys as sysdba" using information in the dba.properties file 
     * to unlock the user specified in the db.properties file.
     * @return 
     */
    public static boolean unlockDBUser() {
        Properties dbaProps = null;

        try (FileInputStream fileInput = new FileInputStream(new File("dba.properties"))) {
            dbaProps = new Properties();
            dbaProps.load(fileInput);
        } catch (FileNotFoundException e) {
            LOG.log(Level.SEVERE, null, e);
        } catch (IOException e) {
            LOG.log(Level.SEVERE, null, e);
        }

        Enumeration props = dbaProps.keys();
        while (props.hasMoreElements()) {
            String key = (String) props.nextElement();
            String value = dbaProps.getProperty(key);
            LOG.log(Level.INFO, "{0}: {1}", new Object[]{key, value});
        }

        final String ip = dbaProps.getProperty("ip");
        final String port = dbaProps.getProperty("port");
        final String service = dbaProps.getProperty("service");
        final String usernameDBA = dbaProps.getProperty("username");
        final String passwordDBA = dbaProps.getProperty("password");

        String urlDBA = "jdbc:oracle:thin:@127.0.0.1:" + port + "/" + service;
        LOG.log(Level.INFO, "Connection url={0}", urlDBA);

        OracleDataSource odsForDBA = null;
        try {
            odsForDBA = new OracleDataSource();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        odsForDBA.setURL(url);
        odsForDBA.setUser(usernameDBA);
        odsForDBA.setPassword(passwordDBA);

        Connection c = null;
        try {
            c = odsForDBA.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DataSourceCreator.class.getName()).log(Level.SEVERE, null, ex);
        }

        Statement s = null;
        try {
            s = c.createStatement();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }

        String qry = "alter user " + username + " identified by " + password + " account unlock";
        LOG.log(Level.INFO, "Query: {0}\n", qry);
        try {
            ResultSet rs = s.executeQuery(qry);
            LOG.info("Query executed correctly.\n");

        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return true;
    }

}
