drop table booksAudit;
drop table booksRowAudit;
drop sequence booksAuditIdSequence;
drop sequence booksRowAuditIdSequence;

CREATE TABLE booksAudit
(booksAuditId NUMBER(5),
actionType VARCHAR2(6),
dateUpdated DATE,
updatingUser VARCHAR2(30),
CONSTRAINT booksAudit_booksAuditId_pk PRIMARY KEY (booksAuditId));

CREATE SEQUENCE booksAuditIdSequence
START WITH 1;

CREATE TABLE booksRowAudit
(booksRowAuditId NUMBER(5),
isbn VARCHAR2(16),
title VARCHAR2(40),
oldPrice Number(8,2),
newPrice Number(8,2),
dateUpdated DATE, 
updatingUser VARCHAR2(30),
CONSTRAINT isbn_fk FOREIGN KEY (isbn) REFERENCES tblBooks(isbn),
CONSTRAINT bksRowAdt_booksRowAuditId_pk PRIMARY KEY (booksRowAuditId ));

CREATE SEQUENCE booksRowAuditIdSequence
START WITH 1;
/

CREATE OR REPLACE TRIGGER booksAuditInsert
 AFTER INSERT ON tblbooks
BEGIN
  INSERT INTO booksAudit VALUES
  (booksAuditIdSequence.NEXTVAL, 'INSERT', SYSDATE, USER);
END;
/

CREATE OR REPLACE TRIGGER booksAuditDelete
 AFTER DELETE ON tblbooks
BEGIN
  INSERT INTO booksAudit VALUES
  (booksAuditIdSequence.NEXTVAL, 'DELETE', SYSDATE, USER);
END;
/

CREATE OR REPLACE TRIGGER booksAuditUpdate
 AFTER UPDATE ON tblbooks
BEGIN
  INSERT INTO booksAudit VALUES
  (booksAuditIdSequence.NEXTVAL, 'UPDATE', SYSDATE, USER);
END;
/

CREATE OR REPLACE TRIGGER booksRowAuditUpdate
 AFTER UPDATE ON tblbooks
 FOR EACH ROW
BEGIN
  INSERT INTO booksRowAudit VALUES 
  (booksRowAuditIdSequence.NEXTVAL, :OLD.isbn, :OLD.title, :OLD.price, 
  :NEW.price, SYSDATE, USER);
END;
/