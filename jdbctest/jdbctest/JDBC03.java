package jdbctest;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JDBC03 {

    private static final Logger LOG = Logger.getLogger(JDBC03.class.getName());

    public static void main(String[] args) {
        Connection conn = DataSourceCreator.getConn();
        Statement dbStatement = null;
        ResultSet dbResultSet = null;

        String query
                = "Select isbn, AuthLastName, AuthFirstName, title, "
                + "price from tblbooks";
        System.out.println(query);

        try {
            dbStatement = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);
            dbResultSet = dbStatement.executeQuery(query);
            System.out.println(
                    "Select statement executed successfully");
        } catch (SQLException e) {
            System.out.println("Problems creating and executing query "
                    + e);
        }

        // Display contents of ResultSet.
        System.out.println();
        System.out.println("CONTENTS OF TBLBOOKS");
        displayResults(dbResultSet);
        System.out.println();

        // Create and execute a query that returns no records; then
        // display results to check that displayResults works for
        // an empty result set. THIS WAS NOT REQUIRED IN THE WORKSHEET.
        query = "Select isbn, AuthLastName, AuthFirstName, title, "
                + "price from tblBooks where isbn = '0'";
        System.out.println(query);

        try {
            dbResultSet = dbStatement.executeQuery(query);
            System.out.println(
                    "Select statement executed successfully");
        } catch (SQLException e) {
            System.out.println("Problems creating and executing query "
                    + e);
        } // end try-catch

        // Display contents of ResultSet.
        System.out.println();
        System.out.println("Contents of result for books set");
        System.out.println("DISPLAYING EMPTY RESULT SET");
        displayResults(dbResultSet);
        System.out.println();

        // Create and execute an SQL statement to insert a book
        // into tblBooks.
        query
                = "insert into tblBooks(isbn, authlastname, authfirstname,"
                + " title, price) values('2', 'McDonald', 'Midge', "
                + "'Obedience Training', 14.95)";
        try {
            dbStatement.executeUpdate(query);
            System.out.println(
                    "Insert statement executed successfully");
        } catch (SQLException e) {
            System.out.println(
                    "Problems creating and executing insert " + e);
        } // end try-catch
        System.out.println();

        // Create and execute an SQL statement to set the price of all
        // books to 15.25
        query = "update tblBooks set price = 15.25";
        try {
            dbStatement.executeUpdate(query);
            System.out.println(
                    "Update statement executed successfully");
        } catch (SQLException e) {
            System.out.println(
                    "Problems creating and executing update " + e);
        } // end try-catch

        // Write an SQL statment to update the price of the book
        // for a specified isbn; new price and isbn are input parameters.
        // Execute using a PreparedStatement, with parameter values
        // input from the keyboard.
        query = "update tblBooks set price = ? where isbn = ?";
        try {
            PreparedStatement updatePrice
                    = conn.prepareStatement(query);
            Scanner in = new Scanner(System.in);
            System.out.print(
                    "Enter isbn of book for which price will be changed: ");
            updatePrice.setString(2, in.next());
            System.out.print("Enter new price: ");
            updatePrice.setDouble(1, in.nextDouble());
            updatePrice.executeUpdate();
            System.out.println("Price for specified isbn "
                    + " updated correctly");
        } catch (SQLException e) {
            System.out.println(""
                    + "Problems executing prepared statement" + e);
        }
        System.out.println();

        // Write and execute a query to retrieve information from tblBooks.
        query = "Select isbn, AuthLastName, AuthFirstName, title, "
                + "price from tblbooks";
        System.out.println(query);

        try {
            dbResultSet = dbStatement.executeQuery(query);
            System.out.println(
                    "Select statement executed successfully");
        } catch (SQLException e) {
            System.out.println("Problems creating and executing query "
                    + e);
        } // end try-catch
        System.out.println();

        // Display contents of ResultSet.
        System.out.println();
        System.out.println("Contents of result for books set");
        displayResults(dbResultSet);
        System.out.println();

        // Create and execute a query to retrieve information from
        // table booksAudit.
        try {
            dbResultSet = dbStatement.executeQuery(
                    "select * from booksAudit");
            System.out.println("Query executed");
            displayAuditResults(dbResultSet);
            System.out.println();
        } catch (SQLException ex) {
            System.out.println("Problems with selecting or displaying"
                    + " information from booksAudit" + ex);
        }
        // Create and execute a query to retrieve information from
        // table booksRowAudit.
        try {
            dbResultSet = dbStatement.executeQuery(
                    "select isbn, title, oldprice, newprice"
                    + " from booksRowAudit");
            displayRowAuditResults(dbResultSet);
            System.out.println();
        } catch (SQLException ex) {
            System.out.println("Problems with selecting or displaying"
                    + " information from booksRowAudit" + ex);
        }

        try {
            dbStatement.close();
            LOG.info("Statement closed.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
        try {
            conn.close();
            LOG.info("Connection closed.\n");
        } catch (SQLException e) {
            LOG.log(Level.SEVERE, null, e);
        }
    } // end main

    public static void displayResults(ResultSet dbrs) {
        try {
            String nameIn;
            if (!dbrs.first()) {
                System.out.println("No records");
            } else {
                do {
                    nameIn = dbrs.getString("authFirstName") + " "
                            + dbrs.getString("authLastName");
                    System.out.printf("%-15s %-35s %-25s $%6.2f",
                            dbrs.getString("isbn"), dbrs.getString("title"),
                            nameIn, dbrs.getDouble("price"));
                    System.out.println();
                } while (dbrs.next());
            } // end else
        } catch (SQLException ex) {
            System.out.println("Problems displaying books");
        }
    } // end displayResults

    private static void displayAuditResults(ResultSet dbrs) {
        try {
            if (!dbrs.first()) {
                System.out.println("No records");
            } else {
                do {
                    System.out.printf("%-5s %-10s %-15s %-10s",
                            dbrs.getString("booksAuditId"), dbrs.getString(
                                    "actionType"),
                            dbrs.getDate("dateUpdated"), dbrs.getString(
                                    "updatingUser"));
                    System.out.println();
                } while (dbrs.next());
            } // end else
        } catch (SQLException ex) {
            System.out.println("Problems displaying audit information");
        }
    }

    private static void displayRowAuditResults(ResultSet dbrs) {
        try {
            if (!dbrs.first()) {
                System.out.println("No records");
            } else {
                do {
                    System.out.printf("%-15s %-35s $%6.2f $%6.2f",
                            dbrs.getString("isbn"), dbrs.getString("title"),
                            dbrs.getDouble("oldPrice"), dbrs.getDouble(
                                    "newPrice"));
                    System.out.println();
                } while (dbrs.next());
            } // end else
        } catch (SQLException ex) {
            System.out.println(
                    "Problems displaying row audit information");
        }
    }

}
