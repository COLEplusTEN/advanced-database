/**
 * Created by Coleten McGuire on 4/19/2016.
 */

import java.sql.*;

public class Main {
    public static void main(String[] args) {
        String url = "jdbc:oracle:thin:@localhost:1521/pdborcl";
        String username = "hr";
        String password = "hr";
        Statement dbStatement = null;
        ResultSet dbResultSet = null;
        Connection conn = null;

        try {
            conn =
                    DriverManager.getConnection(url, username, password);
            System.out.println("Connection successful.");
        } catch (SQLException ex) {
            System.out.println("Problems with connection" + ex);
        }

        String query =
                "SELECT DOC_ID FROM DOCTOR";
        System.out.println("Query: " + query);
        try {
            dbStatement =
                    conn.createStatement();
            System.out.println("Statement created successfully.");
        } catch (SQLException ex) {
            System.out.println("Problems creating statement" + ex);
        }
        try {
            dbResultSet = dbStatement.executeQuery(query);
            System.out.println("Query executed correctly.");
        } catch (SQLException ex) {
            System.out.println("Problems executing statement" + ex);
        }
        System.out.println();
        displayResults(dbResultSet);
        System.out.println();
        try {
            dbStatement.close();
            System.out.println("Statement closed.");
        } catch (SQLException ex) {
            System.out.println("Problem closing statement.");
        }
        try {
            conn.close();
            System.out.println("Connection closed.");
        } catch (SQLException ex) {
            System.out.println("Problem closing connection.");
        }
    } // end JavaOracle

    public static void displayResults(ResultSet dbrs) {
        int count = 0;
        try {
            while (dbrs.next()) {
                System.out.printf("%s%n",
                        dbrs.getInt(1));
                count++;
            }
        } catch (SQLException ex) {
            System.out.println(
                    "SQLException occurred while displaying results.\n" +
                            ex);
        }
        System.out.println();
        System.out.println(count + " records in result set");
    }
}