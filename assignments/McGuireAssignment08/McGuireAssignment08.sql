-- drop tables
drop table PAYMENT_TABLE;
drop table BILL_TABLE;
drop table APPOINTMENT_TABLE;
drop table PATIENT_TABLE;
drop table INSURANCECOMPANY_TABLE;
drop table DOCTOR_TABLE;

-- create DOCTOR_TABLE table
create table DOCTOR_TABLE (
  DOC_ID number not null,
  DOC_FIRSTNAME varchar2(4000),
  DOC_LASTNAME varchar2(4000),
  primary key (DOC_ID)
);

-- create INSURANCECOMPANY_TABLE table
create table INSURANCECOMPANY_TABLE (
  INSURANCE_ID number not null,
  BENEFITS varchar2(4000),
  MOBILE_NUMBER number,
  CLAIMS_ADDRESS varchar2(4000),
  primary key (INSURANCE_ID)
);

-- create PATIENT_TABLE table
create table PATIENT_TABLE (
  PATIENT_ID number not null,
  PATIENT_FIRSTNAME varchar2(4000),
  PATIENT_LASTNAME varchar2(4000),
  ADDRESS varchar2(4000),
  CITY varchar2(4000),
  STATE varchar2(4000),
  INSURANCE_ID number references INSURANCECOMPANY_TABLE(INSURANCE_ID),
  primary key (PATIENT_ID)
);

-- create APPOINTMENT_TABLE table
create table APPOINTMENT_TABLE (
  APPOINTMENT_ID number not null,
  APPOINTMENT_DATE date,
  APPOINTMENT_TIME varchar2(4000),
  APPOINTMENT_DURATION number,
  APPOINTMENT_REASON varchar2(4000),
  DOC_ID number references DOCTOR_TABLE(DOC_ID),
  PATIENT_ID number references PATIENT_TABLE(PATIENT_ID),
  primary key (APPOINTMENT_ID)
);

-- create BILL_TABLE table
create table BILL_TABLE (
  BILL_ID number not null,
  AMOUNT_INSURED number,
  AMOUNT_UNINSURED number,
  DATE_SENT date,
  BILL_STATUS varchar2(4000),
  APPOINTMENT_ID number references APPOINTMENT_TABLE(APPOINTMENT_ID),
  primary key (BILL_ID)
);

-- create PAYMENT_TABLE table
create table PAYMENT_TABLE (
  PAYMENT_ID number not null,
  AMOUNT number,
  PAYMENT_DATE date,
  PAYMENT_METHOD varchar2(4000),
  INSURANCE_ID number references INSURANCECOMPANY_TABLE(INSURANCE_ID),
  BILL_ID number references BILL_TABLE(BILL_ID),
  PATIENT_ID number references PATIENT_TABLE(PATIENT_ID),
  primary key (PAYMENT_ID)
);
