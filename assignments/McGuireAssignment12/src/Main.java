/**
 * Created by Coleten McGuire on 4/20/2016.
 */
import java.sql.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        //4)	Asks the user for connection information and connects to the database appropriately
        Scanner input = new Scanner(System.in);
        System.out.print("Enter url: ");
        String url = input.nextLine();
        System.out.print("Enter username: ");
        String username = input.nextLine();
        System.out.print("Enter password: ");
        String password = input.nextLine();
        Statement dbStatement = null;
        ResultSet dbResultSet = null;
        Connection conn = null;

        try {
            conn =
                    DriverManager.getConnection(url, username, password);
            System.out.println("Connection successful.");
        } catch (SQLException ex) {
            System.out.println("Problems with connection" + ex);
        }
        //6)	Creates a statement to execute a query that returns the APPOINTMENT_ID, APPOINTMENT_DATE,  APPOINTMENT_TIME,
        // APPOINTMENT_REASON, DOC_ID, PATIENT_ID of all patients in appointment table
        String query = "SELECT APPOINTMENT_ID, APPOINTMENT_DATE, APPOINTMENT_TIME, APPOINTMENT_REASON, DOC_ID, PATIENT_ID" +
                " FROM APPOINTMENT";
        System.out.println("Query: " + query);
        //5)	Calls the stored procedure “addNewDoctor” to create a new doctor, named “Joel(as First Name), Wallach(as Last Name)”
        // specialized in oncology with phone number “4254496223” calls the stored procedure “deleteDoctor”  to delete the existing
        // doctor, with the id “7779”
        try {
            CallableStatement addNewDoctor = conn.prepareCall("call addNewDoctor('Wallach', 'Joel', 'Oncology', 4254496223)");
            CallableStatement deleteDoctor = conn.prepareCall(("call deleteDoctor(7779)"));
            System.out.println("CallableStatement created");
            addNewDoctor.executeUpdate();
            deleteDoctor.executeUpdate();
            System.out.println("CallableStatement executed");
        } catch (SQLException ex) {
            System.out.println("Problems with CallableStatement");
        }
        try {
            dbStatement = conn.createStatement();
            System.out.println("Statement created successfully.");
        } catch (SQLException ex) {
            System.out.println("Problems creating statement" + ex);
        }
        try {
            dbResultSet = dbStatement.executeQuery(query);
            System.out.println();
            displayResults(dbResultSet);
            System.out.println();
            System.out.println("Query executed correctly.");
        } catch (SQLException ex) {
            System.out.println("Problems executing statement" + ex);
        }
        try {
            dbStatement.close();
            System.out.println("Statement closed.");
        } catch (SQLException ex) {
            System.out.println("Problem closing statement.");
        }
        try {
            conn.close();
            System.out.println("Connection closed.");
        } catch (SQLException ex) {
            System.out.println("Problem closing connection.");
        }
    } // end JavaOracle

    public static void displayResults(ResultSet dbrs) {
        int count = 0;
        try {
            System.out.printf("%-10s %-10s %-10s %-10s %-10s %-10s%n", "APPOINTMENT_ID", "APPOINTMENT_DATE",
                    "APPOINTMENT_TIME", "APPOINTMENT_REASON", "DOC_ID", "PATIENT_ID");
            while (dbrs.next()) {
                System.out.printf("%-15d %-15s %-20s %-15s %-25d %-15d%n",
                        dbrs.getInt(1), dbrs.getDate(2), dbrs.getString(3), dbrs.getString(4), dbrs.getInt(5), dbrs.getInt(6));
                count++;
            }
        } catch (SQLException ex) {
            System.out.println(
                    "SQLException occurred while displaying results.\n" +
                            ex);
        }
        System.out.println();
        System.out.println(count + " records in result set");
    }
}