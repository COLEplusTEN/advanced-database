--1)	Write a sequence that generates DOC_ID. The name of the sequence should be “generateDoctorID”. The sequence should start with “8000”. You will use the sequence in the procedure “addNewDoctor” to insert DOC_ID.
drop sequence generateDoctorID;
create sequence generateDoctorID start with 8000 increment by 1;

--2)	Write a stored procedure “addNewDoctor” that creates a new doctor.  The procedure should accept  DOC_ID, DOC_LASTNAME, DOC_FIRSTNAME, SPECIALITY and MOBILE_NUMBER as parameters. Also - create a Trigger named “newDoctor” to print to the console(“A new doctor is added.”), whenever a doctor is added in the database.
create or replace procedure addNewDoctor (docLastName in VARCHAR2, docFirstName in VARCHAR2, spec in VARCHAR2, mobile in number
)
AS
begin
  insert into DOCTOR values(generateDoctorID.nextval, docLastName, docFirstName, spec, mobile);
end;
/
create or replace trigger newDoctor
after
insert
on DOCTOR
begin
  dbms_output.put_line('A new doctor is added');
end;
/

--3)	Write a stored procedure “deleteDoctor” that deletes an existing doctor.  The procedure should accept an id (doctor id) as an input parameter. Also - create a trigger named “doctor_delete” to print to the console(”A doctor has been deleted from the Doctor table”), whenever a doctor is deleted from the database.
create or replace procedure deleteDoctor(
  docID in number
)
AS
begin
  delete from DOCTOR
  where DOC_ID = docID;
end;
/
create or replace trigger doctor_delete
after
delete
on DOCTOR
begin
  dbms_output.put_line('A doctor has been deleted from the Doctor table');
end;
/
