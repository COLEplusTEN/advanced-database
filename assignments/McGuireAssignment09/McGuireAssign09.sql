--CREATING DOCTOR TABLE
DROP TABLE PAYMENT;
DROP TABLE APPOINTMENT;
DROP TABLE BILL;
DROP TABLE PATIENT;
DROP TABLE INSURANCE_COMPANY;
DROP TABLE DOCTOR;

CREATE TABLE DOCTOR
(
DOC_ID NUMBER NOT NULL,
DOC_LASTNAME VARCHAR2(50),
DOC_FIRSTNAME VARCHAR2(50),
SPECIALITY VARCHAR2(50),
MOBILE_NUMBER NUMBER,
PRIMARY KEY(DOC_ID)
);

--CREATING INSURANCECOMPANYTABLE
CREATE TABLE INSURANCE_COMPANY
(
INSURANCE_ID NUMBER NOT NULL,
COMPANY_NAME VARCHAR(50),
INSURANCE_NAME VARCHAR(50),
CONTACT_NUMBER NUMBER,
CLAIM_ADDRESS VARCHAR(150),
CITY VARCHAR2(50),
STATE VARCHAR2(10),
PRIMARY KEY(INSURANCE_ID)
);

--CREATING PATIENT TABLE
CREATE TABLE PATIENT
(
PATIENT_ID NUMBER NOT NULL,
FIRST_NAME VARCHAR2(50),
LAST_NAME VARCHAR2(50),
DOB DATE,
ADDRESS VARCHAR2(150),
CITY VARCHAR2(50),
STATE VARCHAR2(10),
INSURANCE_ID NUMBER,
FOREIGN KEY (INSURANCE_ID) REFERENCES INSURANCE_COMPANY(INSURANCE_ID),
PRIMARY KEY(PATIENT_ID)
);

--CREATING APPOINTMENT TABLE
CREATE TABLE APPOINTMENT
(
APPOINTMENT_ID NUMBER NOT NULL,
APPOINTMENT_DATE DATE,
APPOINTMENT_TIME VARCHAR2(50),
APPOINTMENT_REASON VARCHAR2(150),
DOC_ID NUMBER,
PATIENT_ID NUMBER,
FOREIGN KEY (DOC_ID) REFERENCES DOCTOR(DOC_ID),
FOREIGN KEY (PATIENT_ID) REFERENCES PATIENT(PATIENT_ID),
PRIMARY KEY(APPOINTMENT_ID)
);

--CREATING BILL TABLE
CREATE TABLE BILL
(
BILL_ID NUMBER NOT NULL,
AMOUNT_INSURED number,
AMOUNT_UNINSURED number,
DATE_SENT DATE,
BILL_STATUS VARCHAR2(50),
PRIMARY KEY(BILL_ID)
);

--CREATING PAYMENT TABLE
CREATE TABLE PAYMENT
(
PAYMENT_ID NUMBER NOT NULL,
AMOUNT NUMBER,
PAYMENT_DATE DATE,
METHOD VARCHAR2(50),
INSURANCE_ID NUMBER,
BILL_ID NUMBER,
PATIENT_ID NUMBER,
PRIMARY KEY(PAYMENT_ID),
FOREIGN KEY (INSURANCE_ID) REFERENCES INSURANCE_COMPANY(INSURANCE_ID),
FOREIGN KEY (BILL_ID) REFERENCES BILL(BILL_ID),
FOREIGN KEY (PATIENT_ID) REFERENCES PATIENT(PATIENT_ID)
);

-- insert data into DOCTOR table
insert into DOCTOR values (62703, 'Campbell', 'Paul', 'Cardiology', 2257115505);
insert into DOCTOR values (62704, 'Howard', 'Walter', 'Gynecology', 4691342517);
insert into DOCTOR values (62705, 'Sullivan', 'Gary', 'Orthopedic', 4432074133);
insert into DOCTOR values (62706, 'McGuire', 'Tammy', 'Proctology', 1234567890);
insert into DOCTOR values (62707, 'Greeley', 'Tyler', 'Pediatric', 2345678901);
insert into DOCTOR values (62708, 'Luke', 'Dillon', 'Podiatric', 3456789012);

-- insert data into INSURANCE_COMPANY table
insert into INSURANCE_COMPANY values (110011237, 'Coventry Health Care of KS Inc', 'Platinum', 4432679029, '72 Lerdahl Road', 'Seattle', 'WA');
insert into INSURANCE_COMPANY values (110011238, 'Coventry Health Care of KS Inc', 'Gold', 4432679029, '72 Lerdahl Road', 'Seattle', 'WA');
insert into INSURANCE_COMPANY values (120311769, 'Humana, Inc.', 'Silver', 5888862124, '51318 Londonderry Street', 'Miami', 'FL');
insert into INSURANCE_COMPANY values (130011009, 'Morrill Insurance Group', 'Family full coverage', 8678808134, '4 Main Junction', 'New York City', 'NY');
insert into INSURANCE_COMPANY values (130011355, 'Morrill Insurance Group', 'Renters', 8678808134, '4 Main Junction', 'New York City', 'NY');

-- insert data into PATIENT table
insert into PATIENT values (60700, 'Coleten', 'McGuire', to_date('12/18/1992', 'MM/DD/YYYY'), '123 Main', 'Maryville', 'MO', 110011237);
insert into PATIENT values (50456, 'Sardar', 'Mohammed', to_date('01/7/1993', 'MM/DD/YYYY'), '1121 N College Drive', 'Maryville', 'MO', 110011237);
insert into PATIENT values (50345, 'Denise', 'Lawson', to_date('06/6/1994', 'MM/DD/YYYY'), '07 Fulton Circle', 'Dallas', 'TX', 120311769);
insert into PATIENT values (50754, 'Raymond', 'Woods', to_date('01/22/2000', 'MM/DD/YYYY'), '49462 Hagan Junction', 'Rochester', 'NY', 130011009);
insert into PATIENT values (66708, 'James', 'Spader', to_date('03/11/1985', 'MM/DD/YYYY'), '32 Sloan Place', 'Orange County', 'CA', 110011238);
insert into PATIENT values (50347, 'Kumar', 'Anvesh', to_date('06/6/1994', 'MM/DD/YYYY'), '619 W 1st', 'Maryville', 'MO', 120311769);
insert into PATIENT values (50337, 'Nandini', 'Vadey', to_date('04/6/1988', 'MM/DD/YYYY'), '678 N 1st', 'Maryville', 'MO', 120311769);
insert into PATIENT values (50336, 'Jethro', 'Tull', to_date('05/8/1991', 'MM/DD/YYYY'), '655 E 1st', 'Maryville', 'MO', 110011237);
insert into PATIENT values (50335, 'Bill', 'Baggins', to_date('04/6/1987', 'MM/DD/YYYY'), '666 S 1st', 'Maryville', 'TN', 110011238);
insert into PATIENT values (50334, 'David', 'Copperfield', to_date('11/2/1988', 'MM/DD/YYYY'), '1400 N College Drive', 'Maryville', 'MO', 120311769);

-- insert data into APPOINTMENT table
insert into APPOINTMENT values (1102, to_date('03/20/2016', 'MM/DD/YYYY'), '3:00 PM', 'Heart Problem', 62703, 50456);
insert into APPOINTMENT values (1103, to_date('03/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);
insert into APPOINTMENT values (1104, to_date('03/20/2016', 'MM/DD/YYYY'), '5:00 PM', 'Knee injury', 62705, 50754);
insert into APPOINTMENT values (1105, to_date('04/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);
insert into APPOINTMENT values (1106, to_date('05/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);
insert into APPOINTMENT values (1107, to_date('06/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);
insert into APPOINTMENT values (1108, to_date('07/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);
insert into APPOINTMENT values (1109, to_date('08/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);
insert into APPOINTMENT values (1110, to_date('09/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);
insert into APPOINTMENT values (1111, to_date('10/20/2016', 'MM/DD/YYYY'), '12:00 PM', 'Regular check up', 62704, 50345);

-- insert data into BILL table
insert into BILL values (61047820, 200.5, 100.01, to_date('03/26/2016', 'MM/DD/YYYY'), 'PAID');
insert into BILL values (50563109, 350.67, 400.07, to_date('03/24/2016', 'MM/DD/YYYY'), 'PENDING');
insert into BILL values (55404001, 450.5, 200.5, to_date('03/23/2016', 'MM/DD/YYYY'), 'PAID');
insert into BILL values (50463109, 350.67, 400.07, to_date('04/24/2016', 'MM/DD/YYYY'), 'PENDING');
insert into BILL values (50363109, 350.67, 400.07, to_date('05/24/2016', 'MM/DD/YYYY'), 'PENDING');
insert into BILL values (50263109, 350.67, 400.07, to_date('06/24/2016', 'MM/DD/YYYY'), 'PENDING');
insert into BILL values (50163109, 350.67, 400.07, to_date('07/24/2016', 'MM/DD/YYYY'), 'PENDING');
insert into BILL values (50663109, 350.67, 400.07, to_date('08/24/2016', 'MM/DD/YYYY'), 'PENDING');
insert into BILL values (50763109, 350.67, 400.07, to_date('09/24/2016', 'MM/DD/YYYY'), 'PENDING');
insert into BILL values (50863109, 350.67, 400.07, to_date('10/24/2016', 'MM/DD/YYYY'), 'PENDING');

-- insert data into PAYMENT table
insert into PAYMENT values (114201, 300.51, to_date('03/27/2016', 'MM/DD/YYYY'), 'CARD', 110011237, 61047820, 50456);
insert into PAYMENT values (115203, 750.74, NULL, NULL, 120311769, 50563109, 50345);
insert into PAYMENT values (116124, 651, to_date('03/23/2016', 'MM/DD/YYYY'), 'CASH', 130011009, 55404001, 50754);
insert into PAYMENT values (114203, 750.74, NULL, NULL, 120311769, 50563109, 50345);
insert into PAYMENT values (113203, 750.74, NULL, NULL, 120311769, 50563109, 50345);
insert into PAYMENT values (112203, 750.74, NULL, NULL, 120311769, 50563109, 50345);
insert into PAYMENT values (111203, 750.74, NULL, NULL, 120311769, 50563109, 50345);
insert into PAYMENT values (116203, 750.74, NULL, NULL, 120311769, 50563109, 50345);
insert into PAYMENT values (117203, 750.74, NULL, NULL, 120311769, 50563109, 50345);
insert into PAYMENT values (118203, 750.74, NULL, NULL, 120311769, 50563109, 50345);

-- Write a single SQL statement that will calculate and display the total amount generated from all payments made by all the patients
select sum(AMOUNT) as "total amount"
from PAYMENT;

-- Write a Single SQL statement that gives the number of patients who are insured under same insurance company. (Choose any one insurance company from the records available in INSURANCE_COMPANY table)
select count(INSURANCE_ID)
from PATIENT
where INSURANCE_ID = 120311769;

-- Write a query to display the Cartesian product of DOCTOR and PATIENT
select *
from DOCTOR cross join PATIENT;

-- Write a query to join INSURANCE_COMPANY and PATIENT based on INSURANCE_ID
select *
from INSURANCE_COMPANY
inner join PATIENT
on INSURANCE_COMPANY.INSURANCE_ID = PATIENT.INSURANCE_ID;

-- Write a query to display unique INSURANCE_ID’s from the PATIENT table
select unique INSURANCE_ID
from PATIENT;

-- Write a query to update the BILL_STATUS of any one bill in the BILL table and then write a statement that reflects this payment in the PAYMENT table by updating the PAYMENT_DATE and METHOD
update BILL
set BILL_STATUS = 'PENDING'
where BILL_ID = 61047820;
update PAYMENT
set PAYMENT_DATE = NULL, METHOD = NULL
where BILL_ID = 61047820;

-- Write a query to delete any one patient based on PATIENT_ID
delete from PATIENT
where PATIENT_ID = 50754;

-- Write a statement to display your LAST_NAME and FIRST_NAME with DOB
select LAST_NAME, FIRST_NAME, DOB
from PATIENT
where PATIENT_ID = 60700;

-- Write a SQL statement that displays patient’s FIRST_NAME, LAST_NAME whose DOB’s occur between 09 JANUARY, 1995 and 17 SEPTEMBER, 2005
select LAST_NAME, FIRST_NAME
from PATIENT
where DOB >= to_date('01/09/1995', 'MM/DD/YYYY')
and
DOB <= to_date('09/17/2005', 'MM/DD/YYYY');

-- Write a SQL statement to display FIRST_NAME, LAST_NAME, COMPANY_NAME, INSURANCE_NAME of the patients
select unique PATIENT.FIRST_NAME, unique PATIENT.LAST_NAME, unique INSURANCE_COMPANY.COMPANY_NAME, unique INSURANCE_COMPANY.INSURANCE_NAME
from PATIENT, INSURANCE_COMPANY;
