--1)	In PL/SQL, you can print to the console using dbms_output.put_line.  For the output to appear, execute the SQL statement set serveroutput on prior to running the PL/SQL procedure.
set serveroutput on

--2)	Anonymous PL/SQL blocks.  You can use an anonymous PL/SQL block if you have some code you only need to execute one time. Write an anonymous PL/SQL block that inserts a record into the INSURANCE_COMPANY table and then prints a message saying that, a record has been added to the INSURANCE_COMPANY table.
begin
  insert into INSURANCE_COMPANY values (3333, 'Soul', 'Silver', 6608530543, '1647 North Clayton', 'Maryville', 'MO');
  dbms_output.put_line('Inserted a record into INSURANCE_COMPANY table');
end;
/

--3)	Write an anonymous PL/SQL block based on the data provided below to insert a record into the Doctor table:  Once the record is inserted into the table, just print a message saying that ‘A Doctor has been successfully added to the DOCTOR table’.
begin
  insert into DOCTOR values (6858, 'Black', 'Martin', 'Cardiologist', 5474893237);
  dbms_output.put_line('A Doctor has been successfully added to the DOCTOR table');
end;
/

--4)	Stored Procedure: We have three new appointments with different patients. Create a stored procedure named ‘Appointments_Insert’ which will insert a record into APPOINTMENT table with the below parameters.
create or replace procedure Appointments_Insert (
  apt_num number,
  apt_date date,
  apt_time varchar2,
  apt_res varchar2,
  doc_id number,
  pat_num number
)
as
begin
  insert into APPOINTMENT values(apt_num, apt_date, apt_time, apt_res, doc_id, pat_num);
    dbms_output.put_line('A new appointment has been added on ' || apt_date || ' at ' || apt_time);
  end;
  /
execute Appointments_Insert(1111, '05-May-16', '12:00 PM', 'Regular check up', 7016, 50345);
execute Appointments_Insert(1112, '21-Nov-16', '1:00 PM', 'Regular check up', 6090, 50345);
execute Appointments_Insert(1113, '18-Dec-16', '2:00 PM', 'Regular check up', 7238, 50345);

--5)	A patient had to cancel his/her appointment for some reason. Write a stored procedure named Appointment_Delete with one input parameter of Number type with the names APPMNT_ID. This procedure should delete the records in APPOINTMENT table for the given parameter values.
create or replace procedure Appointment_Delete (
  APPMNT_ID number
)
as
id_count number;
begin
select count(*) into id_count from APPOINTMENT where APPOINTMENT_ID = APPMNT_ID;
if id_count > 0 then
begin
  delete from APPOINTMENT where APPOINTMENT_ID = APPMNT_ID;
  dbms_output.put_line('All records have been deleted with the values APPMNT_ID: ' || APPMNT_ID);
end;
end if;
if id_count = 0 then
begin
  dbms_output.put_line('No records have been deleted as there are no matching records for the provided values APPMNT_ID: ' || APPMNT_ID);
end;
end if;
end;
/
execute Appointment_Delete(1111);
execute Appointment_Delete(9999);

--7)	Modify the above stored procedure to add the validations below. The stored procedure should accept only positive numbers as values for input parameters. If the user enters negative values for any of the parameter, it should return the message ‘There should be no negative values in arguments. Please try with positive numbers.’ The procedure should work as earlier if the user enters positive value for the parameter.
create or replace procedure Appointment_Delete (
  APPMNT_ID number
)
as
id_count number;
begin
if APPMNT_ID < 0 then
begin
  dbms_output.put_line('There should be no negative values in arguments. Please try with positive numbers.');
end;
end if;
select count(*) into id_count from APPOINTMENT where APPOINTMENT_ID = APPMNT_ID;
if id_count > 0 then
begin
  delete from APPOINTMENT where APPOINTMENT_ID = APPMNT_ID;
  dbms_output.put_line('All records have been deleted with the values APPMNT_ID: ' || APPMNT_ID);
end;
end if;
if id_count = 0 then
begin
  dbms_output.put_line('No records have been deleted as there are no matching records for the provided values APPMNT_ID: ' || APPMNT_ID);
end;
end if;
end;
/
execute Appointment_Delete(-1111);

--8)	Create a stored procedure named Insurance_Company_Proc to return the INSURANCE_ID, COMPANY_NAME, CONTACT_NUMBER with the below criteria. You should be able to display only the records which satisfy ALL the criteria below for each record: City name GAINESVILLE, state name FL
create or replace procedure Insurance_Company_Proc (
  c varchar2,
  s varchar2
)
as
INSUR_ID number;
COMP_NAME varchar(50);
CONT_NUMBER number;
begin
  select INSURANCE_ID, COMPANY_NAME, CONTACT_NUMBER
  into INSUR_ID, COMP_NAME, CONT_NUMBER
  from INSURANCE_COMPANY
  where city = c AND state = s;
  dbms_output.put_line('INSURANCE_ID is ' || INSUR_ID || ' COMPANY_NAME is ' || COMP_NAME || ' CONTACT_NUMBER is ' || CONT_NUMBER);
end;
/
execute Insurance_Company_Proc('GAINESVILLE', 'FL');

--9)	Create a stored procedure named Patient_Details which displays FIRST_NAME, LAST_NAME, DOB and COMPANY_NAME, CITY of the insurance company by which the patient is insured. You must pass in a parameter PTNT_Number of the number type for PATIENT_ID in the procedure.
create or replace procedure Patient_Details (
  PTNT_Number number
)
as
F_Name VARCHAR2(50);
L_Name VARCHAR2(50);
birth date;
COMP_NAME VARCHAR(50);
c VARCHAR2(50);
begin
  select PATIENT.FIRST_NAME, PATIENT.LAST_NAME, PATIENT.DOB, INSURANCE_COMPANY.COMPANY_NAME, INSURANCE_COMPANY.CITY
  into F_Name, L_Name, birth, COMP_NAME, c
  from PATIENT, INSURANCE_COMPANY
  where PATIENT.PATIENT_ID = PTNT_Number
  AND
  PATIENT.INSURANCE_ID = INSURANCE_COMPANY.INSURANCE_ID;
  dbms_output.put_line('Patients name is  ' || L_Name || ',' || F_NAME || ', his/her birthday is on ' || birth || ', name of the company he is insured by is ' || COMP_Name || ' located at ' || c);
end;
/
execute Patient_Details(50754);

--11)	Create and execute a stored procedure named preciousPatients that displays the patient lastname, patient’s first name, city, payment_date, payment_method for all patients whose amounts are greater than 500.
create or replace procedure preciousPatients
as
lastName PATIENT.LAST_NAME%type;
firstName PATIENT.FIRST_NAME%type;
city PATIENT.CITY%type;
payDate PAYMENT.PAYMENT_DATE%type;
payMethod PAYMENT.PAYMENT_METHOD%type;
amnt PAYMENT.AMOUNT%type;
cursor amount is
  select PATIENT.LAST_NAME, PATIENT.FIRST_NAME, PATIENT.CITY, PAYMENT.PAYMENT_DATE, PAYMENT.PAYMENT_METHOD, PAYMENT.AMOUNT
  from PATIENT inner join PAYMENT
  on PATIENT.PATIENT_ID = PAYMENT.PATIENT_ID and PAYMENT.AMOUNT > 500;
begin
  open amount;
  loop
    fetch amount into lastName, firstName, city, payDate, payMethod, amnt;
    exit when amount%NOTFOUND;
    dbms_output.put_line('Last Name: ' || lastName || ' First Name: ' || firstName || ' City: ' || city || ' Date of payment: '
     || payDate || ' Method: ' || payMethod || ' Amount: ' || amnt);
  end loop;
  dbms_output.put_line('Total number of precious patients: ' || amount%ROWCOUNT);
close amount;
end;
/
execute preciousPatients();

--12)	Create and execute a stored procedure named patientsWithDues which prints the total amount due by each Patient in descending order whose PAYMENT_DATE == “NULL” && PAYMENT_METHOD == “DUE”. Also, print the number of records returned by your query. Your code must work for any valid data
create or replace procedure patientsWithDues
as
lastName PATIENT.LAST_NAME%type;
firstName PATIENT.FIRST_NAME%type;
city PATIENT.CITY%type;
amnt PAYMENT.AMOUNT%type;
cursor amountDue is
  select PATIENT.LAST_NAME, PATIENT.FIRST_NAME, PATIENT.CITY, PAYMENT.AMOUNT
  from PATIENT inner join PAYMENT
  on PATIENT.PATIENT_ID = PAYMENT.PATIENT_ID and PAYMENT.PAYMENT_DATE is NULL and PAYMENT.PAYMENT_METHOD = 'DUE'
  order by PAYMENT.AMOUNT DESC;
begin
  open amountDue;
  loop
  fetch amountDue into lastName, firstName, city, amnt;
  exit when amountDue%NOTFOUND;
  dbms_output.put_line('Last Name: ' || lastName || ' First Name: ' || firstName || ' City: ' || city || ' Amount: ' || amnt);
end loop;
dbms_output.put_line('Total number of patients whose payments are due: ' || amountDue%ROWCOUNT);
close amountDue;
end;
/
execute patientsWithDues();
